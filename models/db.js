const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/node', { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if(!err) { console.log('Mongo is connected successfuly') }
    else { console.log('Error in db connection '+ err) }
});

require('./user.model');