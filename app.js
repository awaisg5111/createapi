require('./models/db');

const express = require('express');
const bodyparser = require('body-parser');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');

const app = express();

// Passport Config
require('./config/passport')(passport);

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

//Body Parser
app.use(bodyparser.urlencoded({ extended: false }));

// Express session
app.use(
    session({
      secret: 'secret',
      resave: true,
      saveUninitialized: true
    })
  );

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

//Global var
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});

//Routes
app.use('/',require('./routes/index'));
app.use('/users',require('./routes/users'));

var request = require('request');

var headers = {
    'accept': '*/*',
    'Content-Type': 'application/json',
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVmMmJmOTJmOWM3MWM1MDAxMjhlNDdjOCIsImVtYWlsIjoiYXdhaXNfbW9uIGF1ZyAzMSAyMDIwIDEzOjU0OjEwIGdtdCswMDAwIChjb29yZGluYXRlZCB1bml2ZXJzYWwgdGltZSlAYXdhaXMuY29tIiwiZmlyc3ROYW1lIjoiYXdhaXMiLCJsYXN0TmFtZSI6ImF3YWlzIiwidXNlck5hbWUiOiJhd2FpcyIsIm1ldGEiOnsicGFyZW50QWNjb3VudCI6IjVmMDVjOGFkNmMzOGU0MDAxMmFlMjE2YiIsImNoaWxkQWNjb3VudCI6IjVmMjE0ZDI1Zjk3M2NhMDAxMjU0MDEyMiJ9LCJyb2xlIjoiYXBpLWFjY2VzcyIsImVudHJ5VHlwZSI6ImFjY291bnQiLCJyb2xlVHlwZSI6ImVudGVycHJpc2UifSwiaWF0IjoxNTk4ODkzNTA2LCJleHAiOjE1OTg5Nzk5MDZ9.xx-_ga2WSw3xB7d_95VjVudr2btaB-QCt0kdTnFG7Cg'
};

// var dataString = '{"userName":"awais","password":"awais123"}';
var dataString = '{"retailPrice": "160","productSKU":"12345","productName":"Wine","taxCode":"4211"}';

var options = {
    url: 'http://54.183.173.225:8000/public-apis-service/api/v1/public/createProduct',
    method: 'POST',
    headers: headers,
    body: dataString
};

function callback(error, response, body) {
    if (!error) {
        console.log(body);
    }
    else {
      console.log(err);
    }
}

request(options, callback);

const PORT = process.env.PORT || 5000;

app.listen(PORT,console.log(`Server is started at port ${PORT}`));